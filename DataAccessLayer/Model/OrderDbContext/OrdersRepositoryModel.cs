﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using prjOrders.Models.OrderDbContext;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.Models
{
    public class OrdersRepositoryModel : IOrdersRepositoryModel
    {
        private OrderContext _context;
        public OrdersRepositoryModel(OrderContext context)
        {
            _context = context;
        }

        public List<OrderItem> OrderItemsByOrder(int orderId)
        {
            return
            _context
            .OrderItem
            .Where(w => w.OrderId == orderId)
            .ToList();
        }

        public List<Order> StarPageOrders()
        {
            return
            _context
            .Order
            .Include(o => o.ProviderIdNav)
            .ToList();
        }

        public List<Order> GetOrdersById(int orderId)
        {
            return
            _context
            .Order
            .Include(o => o.ProviderIdNav)
            .Include(o => o.OrderItemIds)
            .Where(w => w.Id == orderId)
            .ToList();
        }

        public Order GetOrder(int orderId)
        {
            return
            _context
            .Order
            .Include(o => o.ProviderIdNav)
            .Include(o => o.OrderItemIds)
            .Where(w => w.Id == orderId)
            .FirstOrDefault();
        }


        public List<Provider> GetProviders()
        {
            return
            _context
            .Provider
            .ToList();
        }

        public int AddOrder(Order order)
        {
            _context
            .Order
            .Add(order);

            _context
                .SaveChanges();
            return order.Id;
        }

        public void DeleteOrder(int orderId)
        {
            var deleteOrder =
                 _context
                 .Order
                 .Include(o => o.OrderItemIds)
                 .Where(o => o.Id == orderId)
                 .ToList();

            if (deleteOrder.Count() > 0)
            {
                _context
                .Order
                .RemoveRange(deleteOrder);
                _context.SaveChanges();
            }

        }

        public Order GetOrderIdByOrderItemId(int orderItemId)
        {
            return
            _context
            .Order
            .Include(o => o.OrderItemIds)
            .Where(w => w.OrderItemIds.Select(w => w.Id).Contains(orderItemId))
            .FirstOrDefault();
        }

        public void DeleteOrderItem(int orderItemId)
        {
            var deleteOrder =
                 _context
                 .OrderItem
                 .Where(o => o.Id == orderItemId)
                 .ToList();

            if (deleteOrder.Count() > 0)
            {
                _context
                .OrderItem
                .RemoveRange(deleteOrder);
                _context.SaveChanges();
            }
        }

        public void SaveChangesOrder(Order order)
        {
            _context
                .Order
                .Update(order);

            _context.SaveChanges();
        }

        public void SaveChangesOrderItems(List<OrderItem> orderItems)
        {
            _context
                .OrderItem
                .UpdateRange(orderItems);

            _context.SaveChanges();
        }

        //public List<Order> SerachOrders(SearchDTO searchDTO)
        //{
        //    return FilterOrders(searchDTO);
        //}

        public IQueryable<Order> OrdersForFilters()
        {
            IQueryable<Order> query =
                _context
                .Order
                .Include(o => o.OrderItemIds)
                .Include(p => p.ProviderIdNav)
                ;

            return query;

            //DateTime myDate = DateTime.ParseExact("01-01-0001", "dd-MM-yyyy",
            //                           System.Globalization.CultureInfo.InvariantCulture);

            //if (filter.AfterDate > myDate)
            //{
            //    query = query.Where(c => c.Date >= filter.AfterDate);
            //}

            //if (filter.BeforeDate > myDate)
            //{
            //    query = query.Where(c => c.Date <= filter.BeforeDate);
            //}

            //if (!string.IsNullOrWhiteSpace( filter.Number) && !string.IsNullOrEmpty(filter.Number))
            //{
            //    query = query
            //        .Where(c => EF.Functions.Like(c.Number, $@"%{filter.Number}%"));
            //}

            //if (filter.Providers != null && filter.Providers.Count>0)
            //{
            //    query = query
            //        .Where(w => filter.Providers.Contains(w.ProviderId));
            //}

            //return query.ToList();

        }

    }
}
