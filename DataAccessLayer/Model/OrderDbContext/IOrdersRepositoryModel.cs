﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.Models.OrderDbContext
{
    public interface IOrdersRepositoryModel
    {
        List<Order> StarPageOrders();
        List<OrderItem> OrderItemsByOrder(int orderId);

        List<Order> GetOrdersById(int orderId);

        Order GetOrder(int orderId);

        List<Provider> GetProviders();
        int AddOrder(Order order);
        void DeleteOrder(int orderId);
        void DeleteOrderItem(int orderItemId);
        Order GetOrderIdByOrderItemId(int orderItemId);
        void SaveChangesOrder(Order order);
        void SaveChangesOrderItems(List<OrderItem> orderItems);
        //List< Order> SerachOrders(SearchDTO searchDTO);

        IQueryable<Order> OrdersForFilters();


    }
}
