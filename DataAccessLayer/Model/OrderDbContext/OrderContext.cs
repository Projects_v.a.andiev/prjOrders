﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace prjOrders.Models.OrderDbContext
{
    public partial class OrderContext : DbContext
    {
        public OrderContext()
        {
            Database.EnsureCreated();
        }

        public OrderContext(DbContextOptions<OrderContext> options)
            : base(options)
        {
        }


        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<Provider> Provider { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Provider>().HasData(
            new Provider[]
            {
                new Provider { Id = 1, Name="Рога и копыта"},
                new Provider { Id = 2, Name="SonyPony"},
                new Provider { Id = 3, Name="DigitalYoYo"}
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.Id)
                  .HasName("PK_Order_Id");

                entity.HasIndex(e => e.ProviderId)
                    .HasName("FK_Order_ProviderId");

                entity.HasMany(order => order.OrderItemIds)
                     .WithOne(orderItem => orderItem.OrdersId)
                     .HasForeignKey(d => d.OrderId)
                     .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(order => order.ProviderIdNav)
                     .WithMany(orderItem => orderItem.OrdersId)
                     .HasForeignKey(d => d.ProviderId)
                     .OnDelete(DeleteBehavior.ClientSetNull);

            });

            modelBuilder.Entity<OrderItem>(entity =>
            {
                entity.HasKey(e => e.Id)
                  .HasName("PK_OrderItem_Id");

                entity.HasIndex(e => e.OrderId)
                    .HasName("FK_OrderItem_OrderId");

                entity.HasOne(order => order.OrdersId)
                     .WithMany(orders => orders.OrderItemIds)
                     .HasForeignKey(d => d.OrderId)
                     .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Provider>(entity =>
            {
                entity.HasKey(e => e.Id)
                  .HasName("PK_Provider_Id");



            });

        }
    }
}
