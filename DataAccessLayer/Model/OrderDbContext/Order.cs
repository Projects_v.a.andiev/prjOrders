﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.Models.OrderDbContext
{
    public class Order
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public DateTime Date { get; set; }

        public int ProviderId { get; set; }

        public virtual Provider ProviderIdNav { get; set; }

        public virtual ICollection<OrderItem> OrderItemIds { get; set; }
    }

    public class OrderItem
    { 
        public int Id { get; set; }

        public int OrderId { get; set; }

        public string    Name { get; set; }
        public Decimal Quantity{ get; set; }

        public string Unit { get; set; }

        public virtual Order OrdersId { get; set; }


    }

    public class Provider
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Order> OrdersId { get; set; }

    }
}
