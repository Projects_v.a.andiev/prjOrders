﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using prjOrders.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using prjOrders.Models.OrderDbContext;
using prjOrders.ViewModel.OrdersModel;
using prjOrders.Models.OrderOperation;
using BusinessLayer.OrderOperation;

namespace prjOrders.Controllers
{

    public class OrdersController : Controller
    {
        private readonly ILogger<OrdersController> _logger;
        List<OrderDTO> _orderDTOs;        
        OrderDTOHelper _orderHelper;
        OrdersRepositoryHelper _ordersRepositoryHelper;

        public OrdersController(ILogger<OrdersController> logger, OrdersRepositoryHelper ordersRepositoryModel, OrderDTOHelper orderDTOHelper)
        {
            _logger = logger;            
            _orderDTOs = new List<OrderDTO>();
            _orderHelper = orderDTOHelper;
            _ordersRepositoryHelper = ordersRepositoryModel;
        }

        public IActionResult Index()
        {
            var providers = _orderHelper.GetProvidersDTO(_ordersRepositoryHelper.GetProviders());
            var result = _orderHelper.FirstPageDTO(_ordersRepositoryHelper.StarPageOrders(), providers);
            var serchDTO = _orderHelper.SearchDTO();

            return View(result);
        }

        [HttpGet]
        public IActionResult ShowOrderInfo(int orderId)
        {
            var order = _ordersRepositoryHelper.GetOrder(orderId);

            if (order is null)
            {
                return RedirectToAction("Index");
            }
            return View(order);
        }

        [HttpGet]
        public IActionResult InputNewOrder()
        {

            var order = _orderHelper.GetOrdersDTO(_ordersRepositoryHelper.GetOrder(0));

            var orderItems = _orderHelper.GetOrderItemsDTO(_ordersRepositoryHelper.OrderItemsByOrder(order.Id));

            var providers = _orderHelper.GetProvidersDTO(_ordersRepositoryHelper.GetProviders());

            return View(_orderHelper.ViewOrders(order, orderItems, providers));
        }

        [HttpPost]
        public IActionResult SaveNewOrder(ViewOrdersInfo viewOrders)
        {
            var order = _orderHelper.GetOrderFromDTO(viewOrders.OrdersDTO);



            return RedirectToAction("ShowOrderInfo", new { orderId = _ordersRepositoryHelper.AddOrder(order) });
        }


        [HttpPost]
        public IActionResult DeleteOrder(int orderId)
        {
            _ordersRepositoryHelper.DeleteOrder(orderId);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult EditDeleteOrder(int orderItemId, ViewOrdersInfo viewOrdersInfo)
        {
            var orderItem = _ordersRepositoryHelper.GetOrderIdByOrderItemId(orderItemId);
            if (orderItem == null)
            {
                return RedirectToAction("EditOrder", new { Id = viewOrdersInfo.OrdersDTO.Id });
            }
            _ordersRepositoryHelper.DeleteOrderItem(orderItemId);
            return RedirectToAction("EditOrder", new { Id = orderItem.Id });
        }

        [HttpGet]
        public IActionResult EditOrder(int id)
        {
            var order = _ordersRepositoryHelper.GetOrder(id);

            if (order is null)
            {
                return RedirectToAction("Index");
            }

            var providers = _orderHelper.GetProvidersDTO(_ordersRepositoryHelper.GetProviders());

            var orders = _orderHelper.GetOrdersDTO(order);

            var orderItems = _orderHelper.GetOrderItemsDTO(_ordersRepositoryHelper.OrderItemsByOrder(order.Id));

            return View(_orderHelper.ViewOrders(orders, orderItems, providers));
        }


        [HttpPost]
        public IActionResult SaveEditOrder(ViewOrdersInfo viewOrdersInfo)
        {
            var order = _orderHelper.GetOrderFromDTO(viewOrdersInfo.OrdersDTO);

            var orderItems = _orderHelper.GetOrderItemFromDTO(viewOrdersInfo.OrderItemsDTO);

            _ordersRepositoryHelper.SaveChangesOrder(order);
            _ordersRepositoryHelper.SaveChangesOrderItems(orderItems);
            return RedirectToAction("EditOrder", new { id = viewOrdersInfo.OrdersDTO.Id });
        }

        [HttpPost]
        public IActionResult AddOrderItem(ViewOrdersInfo viewOrdersInfo)
        {
            var view = _orderHelper.AddOrderItem(viewOrdersInfo);

            return View("EditOrder", view);
        }

        [HttpGet]
        public IActionResult SerachOrders(ViewOrdersDTO viewOrdersDTO)
        {
            var foundOrders  = _ordersRepositoryHelper.FiltersForOrders(viewOrdersDTO.SearchDTO );
            viewOrdersDTO.Orders = foundOrders ;
            viewOrdersDTO.ProvidersDTO = _orderHelper.GetProvidersDTO(_ordersRepositoryHelper.GetProviders());
            return View("Index", viewOrdersDTO);
        }
    } 
}
