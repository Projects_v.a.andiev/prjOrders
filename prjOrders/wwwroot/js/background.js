﻿
window.onload = () => {
    document.querySelector(".classOnlyNumber").addEventListener("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
            evt.preventDefault();
        }
    });
};

$(document).ready(function () {
        $(".bg").css("background-image", "url('../background/img/" + (Math.floor(Math.random() * 6) + 1) + ".jpg')");
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();