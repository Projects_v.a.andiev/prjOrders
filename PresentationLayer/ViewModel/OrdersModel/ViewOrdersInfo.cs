﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.ViewModel.OrdersModel
{
    public class ViewOrdersInfo
    {
        public OrderDTO OrdersDTO { get; set; }
        public List<OrderItemDTO> OrderItemsDTO { get; set; }
        public List<ProviderDTO> ProvidersDTO { get; set; }
    }
}
