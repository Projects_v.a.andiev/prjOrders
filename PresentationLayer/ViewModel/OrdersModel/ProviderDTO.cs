﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.ViewModel.OrdersModel
{
    public class ProviderDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
