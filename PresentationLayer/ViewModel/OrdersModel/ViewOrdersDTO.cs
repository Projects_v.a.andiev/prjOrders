﻿using prjOrders.Models.OrderDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.ViewModel.OrdersModel
{
    public class ViewOrdersDTO
    {
        public List<Order> Orders { get; set; }        
        public List<ProviderDTO> ProvidersDTO { get; set; }

        public SearchDTO SearchDTO { get; set; }
    }
}
