﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.ViewModel.OrdersModel
{
    public class SearchDTO
    {
        public string Number { get; set; }

        public DateTime BeforeDate { get; set; }

        public DateTime AfterDate { get; set; }

        public List<int> Providers { get; set; }
    }
}
