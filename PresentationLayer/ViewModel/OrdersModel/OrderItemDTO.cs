﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.ViewModel.OrdersModel
{
    public class OrderItemDTO
    {
        public int Id { get; set; }

        public int OrderId { get; set; }

        public string Name { get; set; }  

        public Decimal Quantity { get; set; }

        public string Unit { get; set; }
    }
}
