﻿using Microsoft.EntityFrameworkCore;
using prjOrders.Models.OrderDbContext;
using prjOrders.ViewModel.OrdersModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.OrderOperation
{
    public class OrdersRepositoryHelper
    {
        private IOrdersRepositoryModel _ordersRepository;
        public OrdersRepositoryHelper(IOrdersRepositoryModel ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }

        public List<Provider> GetProviders()
        {
            return _ordersRepository.GetProviders();
        }

        public List<Order> StarPageOrders()
        {
            return _ordersRepository.StarPageOrders();
        }

        public Order GetOrder(int orderId)
        {
            return _ordersRepository.GetOrder(orderId);
        }

        public List<Order> FiltersForOrders(SearchDTO filter)
        {
            DateTime myDate = DateTime.ParseExact("01-01-0001", "dd-MM-yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var query = _ordersRepository.OrdersForFilters();

            if (filter.AfterDate > myDate)
            {
                query = query.Where(c => c.Date >= filter.AfterDate);
            }

            if (filter.BeforeDate > myDate)
            {
                query = query.Where(c => c.Date <= filter.BeforeDate);
            }

            if (!string.IsNullOrWhiteSpace(filter.Number) && !string.IsNullOrEmpty(filter.Number))
            {
                query = query
                    .Where(c => EF.Functions.Like(c.Number, $@"%{filter.Number}%"));
            }

            if (filter.Providers != null && filter.Providers.Count > 0)
            {
                query = query
                    .Where(w => filter.Providers.Contains(w.ProviderId));
            }

            return query.ToList();
        }

        public List<OrderItem> OrderItemsByOrder(int id)
        {
            return _ordersRepository.OrderItemsByOrder(id);
        }

        public int AddOrder(Order order)
        {
            return _ordersRepository.AddOrder(order);
        }

        public void DeleteOrder(int orderId)
        {
            _ordersRepository.DeleteOrder(orderId);
        }

        public Order GetOrderIdByOrderItemId(int orderItemId)
        {
            return _ordersRepository.GetOrderIdByOrderItemId(orderItemId);
        }

        public void SaveChangesOrder(Order order)
        {
            _ordersRepository.SaveChangesOrder(order);
        }

        public void SaveChangesOrderItems(List<OrderItem> orderItems)
        {
            _ordersRepository.SaveChangesOrderItems(orderItems);
        }

        public void DeleteOrderItem(int orderItemId)
        {
            _ordersRepository.DeleteOrderItem(orderItemId);
        }
    }
}
