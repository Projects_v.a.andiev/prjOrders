﻿using AutoMapper;
using prjOrders.Models.OrderDbContext;
using prjOrders.ViewModel.OrdersModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prjOrders.Models.OrderOperation
{
    public class OrderDTOHelper
    {
        private readonly IMapper _mapper;
        public OrderDTOHelper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public OrderDTO GetOrdersDTO(Order order)
        {
            if (order == null)
            {
                return new OrderDTO { };

            }


            var ordersDTO = _mapper.Map<OrderDTO>(order);
            return ordersDTO;
        }

        public List<OrderDTO> GetOrdersDTO(List<Order> orders)
        {

            var ordersDTO = new List<OrderDTO>();

            ordersDTO = _mapper.Map<List<OrderDTO>>(orders);
            return ordersDTO;
        }


        public List<OrderItemDTO> GetOrderItemsDTO(List<OrderItem> orderItems)
        {
            var orderItemsDTO = new List<OrderItemDTO>();

            orderItemsDTO = _mapper.Map<List<OrderItemDTO>>(orderItems);

            return orderItemsDTO;
        }

        public SearchDTO SearchDTO()
        {
            var searchDTO = new SearchDTO { };
            return searchDTO;
        }



        public List<ProviderDTO> GetProvidersDTO(List<Provider> providers)
        {
            var providerDTO = new List<ProviderDTO>();

            providerDTO = _mapper.Map<List<ProviderDTO>>(providers);

            return providerDTO;
        }

        public List<OrderItemDTO> GetOrdersItemsDTO(List<OrderItem> orderItems)
        {
            var orderItemsDTO = new List<OrderItemDTO>();

            orderItemsDTO = _mapper.Map<List<OrderItemDTO>>(orderItems);

            if (orderItemsDTO.Count() < 1)
            {
                orderItemsDTO.Add(new OrderItemDTO());
            }

            return orderItemsDTO;
        }

        public ViewOrdersInfo ViewOrders(OrderDTO orderDTO, List<OrderItemDTO> orderItemsDTO, List<ProviderDTO> providersDTO)
        {
            var viewOrders = new ViewOrdersInfo();
            viewOrders.OrdersDTO = orderDTO;
            viewOrders.OrderItemsDTO = orderItemsDTO;
            viewOrders.ProvidersDTO = providersDTO;

            return viewOrders;
        }

        public Order GetOrderFromDTO(List<OrderDTO> ordersDTO)
        {
            var newOrder = new Order();

            foreach (var order in ordersDTO)
            {
                newOrder.Number = order.Number;
                newOrder.ProviderId = order.ProviderId;
                newOrder.Date = order.Date;
            }

            return newOrder;
        }


        public Order GetOrderFromDTO(OrderDTO ordersDTO)
        {
            var newOrder = new Order();

            newOrder.Id = ordersDTO.Id;
            newOrder.Number = ordersDTO.Number;
            newOrder.ProviderId = ordersDTO.ProviderId;
            newOrder.Date = ordersDTO.Date;

            return newOrder;
        }

        public List<OrderItem> GetOrderItemFromDTO(List<OrderItemDTO> orderItemsDTO)
        {
            var orderItem = new List<OrderItem>();

            orderItem = _mapper.Map<List<OrderItem>>(orderItemsDTO);

            return orderItem;
        }

        public ViewOrdersInfo AddOrderItem(ViewOrdersInfo viewOrdersInfo)
        {
            if (viewOrdersInfo.OrderItemsDTO == null)
            {
                viewOrdersInfo.OrderItemsDTO = new List<OrderItemDTO>();
            }
            viewOrdersInfo.OrderItemsDTO.Add(new OrderItemDTO { OrderId = viewOrdersInfo.OrdersDTO.Id, Name = "", Unit = "" });
            return viewOrdersInfo;
        }

        public ViewOrdersDTO FirstPageDTO(List<Order> orders, List<ProviderDTO> providersDTO)
        {
            var ordersDTO = new ViewOrdersDTO();
            ordersDTO.Orders = orders;
            ordersDTO.ProvidersDTO = providersDTO;

            return ordersDTO;
        }
    }
}
