﻿using AutoMapper;
using prjOrders.Models.OrderDbContext;
using prjOrders.ViewModel.OrdersModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.OrderOperation
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            
            CreateMap<Provider, ProviderDTO>();
            //.ForMember(dest => dest.Id,
            //    opt => opt.MapFrom(src => src.Id))
            //.ForMember(dest => dest.Name,
            //    opt => opt.MapFrom(src => src.Name));   

            CreateMap<ProviderDTO, Provider>();

            CreateMap<Order, OrderDTO>();
            CreateMap<OrderDTO, Order>();


            CreateMap<OrderItemDTO, OrderItem>();
            CreateMap<OrderItem, OrderItemDTO>();
        }
    }
}
